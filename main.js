const express = require('express');
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.json());

let tasks = [
    {id: "1", title: 'Tisch decken', description: 'Decke den Tisch schön bis ich nach Hause komme', done: false, duedate: '2023-08-13'},
    {id: "2", title: 'Zimmer aufräumen', description: 'Räume dein Zimmer so auf das nichts mehr rumliegt', done: false, duedate: '2023-06-29'},
    {id: "3", title: 'Badezimmer putzen', description: 'Putze das Badezimmer gründlich, vorallem die Badewanne', done: false, duedate: '2023-07-03'},
    {id: "4", title: 'Paket zur Post bringen', description: 'Bringe das Zalando Paket zur Post', done: false, duedate: '2023-12-09'},

];

app.get('/tasks', (request, response) => {
    response.status(200).json(tasks);
});

app.post('/tasks', (request, response) => {
    const newTask = request.body;
    const id = tasks.length + 1
    newTask.id = id;
    tasks.push(newTask)
  
    response.status(201).json(newTask);
});


app.get('/tasks/:id', (request, response) => {
    const id = request.params.id;
    const task = tasks.find((task) => task.id === id);
  
    if (!tasks) {
		return response.status(404).send('Task not found')
	} else {
		return response.status(200).send(task)
	}
    
});

app.put('/task/:id', (request, response) => {
    const taskId = parseInt(request.params.id);
    const taskIndex = tasks.findIndex(task => task.id === taskId);

    if (taskIndex !== -1) {
      tasks[taskIndex] = { ...tasks[taskIndex], ...request.body };
      response.json(tasks[taskIndex]);
    } else {
      response.status(404).json({ error: `Task not found` });
    }
});



app.delete('/task/:id', (request, response) => {
    const id =request.params.id;
    const taskIndex = tasks.findIndex(task =>task.id ==id)
  
    if(taskIndex < 0) {
        return response.sendStatus(404).json({ })
    }
  
    tasks.splice(tasks, 1)
    response.sendStatus(204)
});









app.listen(3000);